# Drupal 8 Prototype.

This a a prototype project samping possible configuration and to demonstrate a working product that can be shared.

## Installing Your Site.
Install [Docker](https://docs.docker.com/install/)

Append the following to `/etc/hosts`:

```
sudo "127.0.0.1 local.farmers.gov" >> /etc/hosts
```

Note there is a small change you have to 
[flush your dns resolver cache](help.dreamhost.com/hc/en-us/articles/214981288-Flushing-your-DNS-cache-in-Mac-OS-X-and-Linux).

To run:

```
docker-compose up
```

This will start up your containers. If you want a detached shell use the -d option.
```
docker-compose up -d
```

Navigate to https://local.farmers.gov
## Methods for Configuration and Migrations

Provided in this project is a db-init folder. Any .sql or .sql.gz files in this directory will be imported alphabetically. See the [mysql image](https://hub.docker.com/_/mysql/) documentation for more details.

A sample db-dump.sql.gz file has been provided for demonstration purposes.

You may also use the config_installer profile. This profile will use the configuration provided from config/sync and install it with a matching UUID.

To get a bash shell execute the following:
```
docker-compose run --rm web bash
```
From there you can:
```
cd /var/www/public

# Dump databases to ~/db-init on the container. This will map to db-init/ on your local.
drush sql-dump --result-file=~/db-init/myfilename.sql --gzip

# Drush config import/exports. Reads/Writes to /config/sync 
# which is mapped to config/sync in your local file system.
drush config-export

# Open interactive shell with drupal.
drush php
```

## A note about drush:
The usdaeas/gov-drupal-dev image comes with drush 8 by default. This is incompatible with newer versions of drupal.
What we have done is installed [Drush Launcher](https://github.com/drush-ops/drush-launcher). You can view the Dockerfile it's a fairly simple change.
The launcher is smart enough that when drush is invoked, it will check for the local version at `vendor/bin/drush`. So:
```
cd /var/www/public
drush status

# Returns Drush version    : 9.0.0
```
However
```
cd /var/www/
drush status

# Returns Drush version          :  8.1.7
```
