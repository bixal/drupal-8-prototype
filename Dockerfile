FROM usdaeas/gov-drupal-dev

# todo: This is very slow. Investigate the best way to speed this up.
RUN yum install nodejs ruby openssl mod_ssl -y

# https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Virtualization/3.2/html/Developer_Guide/Creating_an_SSL_Certificate.html
RUN touch /etc/pki/CA/index.txt
RUN echo '1000' > /etc/pki/CA/serial
RUN mkdir /etc/ssl/private/

# We added this for html5 geolocation support which requires ssl. We know this would require substantial changes to images or the ability
# to deploy ours. It is probably an upcoming task nobody has considered. But this works for the demo purposes.
RUN openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt \
    -subj "/C=US/ST=DC/L=local/O=Someone/OU=IT/CN=local.farmers.gov"

RUN openssl dhparam -out /etc/ssl/certs/dhparam.pem 2048
ADD httpd/etc/conf.d/ /etc/httpd/conf.d/

RUN gem install compass scss_lint
RUN npm -g install gulp-cli
# Drush Launcher
RUN wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/download/0.5.1/drush.phar
RUN chmod +x drush.phar
RUN mv drush.phar /usr/local/bin/drush9
# Drupal Launcher
RUN curl https://drupalconsole.com/installer -L -o drupal.phar
RUN mv drupal.phar /usr/local/bin/drupal
RUN chmod +x /usr/local/bin/drupal

# Process management since we have to restart apache for ssl.
COPY conf/run.sh /run.sh
RUN chmod +x /run.sh
CMD ["/run.sh"]