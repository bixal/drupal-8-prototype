$(document).ready(function(){
    initGeolocate();
});

function initGeolocate() {
    if (!Cookies.get('my-location')) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                Cookies.set('my-location', pos, {expires: 90});
                if (!Cookies.get('my-location')) {
                    console.log('Cookies appear to be disabled or something has gone wrong. Please enable cookies or' +
                        'report this error to an administrator.');
                } else {
                    window.location.reload();
                }
                
            }, function() {
                handleLocationError(true);
            });
        } else {
            // Browser doesn't support Geolocation
            handleLocationError(false);
        }
    }
}

function handleLocationError(browserHasGeolocation) {
    if (browserHasGeolocation) {
        console.log('Browser supports Geolocation but there was an error.');
    } else {
        console.log('Browser does not support Geolocation.');
    }
}