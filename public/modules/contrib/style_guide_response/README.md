## Bixal Style guide response

This module serves a style-guide from your themes folder into a URL that you can hit. The code will search for an HTML
file named (you need to generate this file) ```style-guide.html``` inside your themes folder and serve it on
```/style-guide``

* Install the module.
  * Make sure you have a file named style-guide.html inside any folder structure within themes
  * Visit http://<your site>/style-guide.


  